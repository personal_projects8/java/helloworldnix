with import <nixpkgs> {}; {
    qpidEnv = stdenvNoCC.mkDerivation {
        name = "my-java-17-enviroment";
        buildInputs = [
            jdk17
        ];
    };
}